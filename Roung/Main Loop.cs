﻿using SFML.Window;
using SFML.Graphics;
using NetEXT.TimeFunctions;
using NetEXT.Input;

namespace Roung
{
    public class MainLoop
    {
        #region Variables
        private RenderWindow _window = null;
        private Clock _frameclock = null;
        private Time _frametime = Time.Zero;
        private ActionMap _map = null;
        private EventSystem<ActionContext> _eventsystem = null;
        private Background _background = new Background();
        private Paddle _toppaddle = null;
        private Paddle _bottompaddle = null;
        private Ball _ball = new Ball();
        private bool _aienabled = true;
        private int _ailevel = 5;
        private Font _gamefont = null;
        private Text _topplayertext = null;
        private Text _bottomplayertext = null;
        #endregion

        #region Contructors
        public MainLoop()
        {
            _window = new RenderWindow(new VideoMode(Consts.WindowSize.X, Consts.WindowSize.Y), "Roung", Styles.Close, new ContextSettings(0, 0, 8));
            _window.SetVerticalSyncEnabled(false);
            _window.SetFramerateLimit(60);
            _window.SetKeyRepeatEnabled(false);
            _toppaddle = new Paddle(0);
            _toppaddle.MaximumRotation = 77.5f;
            _toppaddle.MinimumRotation = -77.5f;
            _bottompaddle = new Paddle(180);
            _bottompaddle.MaximumRotation = 257.5f;
            _bottompaddle.MinimumRotation = 102.5f;
            SetupActions();
            _gamefont = new Font(".\\Square.ttf");
            _topplayertext = new Text("", _gamefont);
            _topplayertext.Color = Color.Black;
            _topplayertext.CharacterSize = 24;
            _topplayertext.Origin = new Vector2f(0, 0);
            _topplayertext.Position = new Vector2f(25, 10);
            _bottomplayertext = new Text("", _gamefont);
            _bottomplayertext.Color = Color.Black;
            _bottomplayertext.CharacterSize = 24;
            _bottomplayertext.Origin = new Vector2f(0, 0);
            _bottomplayertext.Position = new Vector2f(25, Consts.WindowSize.Y - 40);
        }
        #endregion

        #region Functions
        private void SetupActions()
        {
            _map = new ActionMap();
            _map[ActionEvents.WindowClosed] = new Action(EventType.Closed) | new Action(Keyboard.Key.Escape, ActionType.ReleaseOnce);
            _map[ActionEvents.TopPaddleRight] = new Action(Keyboard.Key.D, ActionType.PressOnce);
            _map[ActionEvents.TopPaddleLeft] = new Action(Keyboard.Key.A, ActionType.PressOnce);
            _map[ActionEvents.TopPaddleStop] = new Action(Keyboard.Key.D, ActionType.ReleaseOnce) | new Action(Keyboard.Key.A, ActionType.ReleaseOnce);
            _map[ActionEvents.BottomPaddleRight] = new Action(Keyboard.Key.Right, ActionType.PressOnce);
            _map[ActionEvents.BottomPaddleLeft] = new Action(Keyboard.Key.Left, ActionType.PressOnce);
            _map[ActionEvents.BottomPaddleStop] = new Action(Keyboard.Key.Right, ActionType.ReleaseOnce) | new Action(Keyboard.Key.Left, ActionType.ReleaseOnce);
            _map[ActionEvents.AIToggle] = new Action(Keyboard.Key.Multiply, ActionType.PressOnce);
            _map[ActionEvents.AILevelUp] = new Action(Keyboard.Key.Add, ActionType.PressOnce);
            _map[ActionEvents.AILevelDown] = new Action(Keyboard.Key.Subtract, ActionType.PressOnce);
            _eventsystem = ActionMap.CreateCallbackSystem();
            _eventsystem.Connect(ActionEvents.WindowClosed, (context) => { _window.Close(); });
            _eventsystem.Connect(ActionEvents.TopPaddleRight, (context) => { if (!_aienabled) _toppaddle.Direction = RotationDirection.Clockwise; });
            _eventsystem.Connect(ActionEvents.TopPaddleLeft, (context) => { if (!_aienabled) _toppaddle.Direction = RotationDirection.CounterClockwise; });
            _eventsystem.Connect(ActionEvents.TopPaddleStop, (context) => { if (!_aienabled) _toppaddle.Direction = RotationDirection.None; });
            _eventsystem.Connect(ActionEvents.BottomPaddleRight, (context) => { _bottompaddle.Direction = RotationDirection.CounterClockwise; });
            _eventsystem.Connect(ActionEvents.BottomPaddleLeft, (context) => { _bottompaddle.Direction = RotationDirection.Clockwise; });
            _eventsystem.Connect(ActionEvents.BottomPaddleStop, (context) => { _bottompaddle.Direction = RotationDirection.None; });
            _eventsystem.Connect(ActionEvents.AIToggle, (context) => { _aienabled = !_aienabled; _toppaddle.Direction = RotationDirection.None; if (!_aienabled) _toppaddle.SpeedMultiplier = 1; else _toppaddle.SpeedMultiplier = (float)_ailevel / 5f; });
            _eventsystem.Connect(ActionEvents.AILevelUp, (context) => { if (_aienabled && _ailevel < 20) _ailevel += 1; _toppaddle.SpeedMultiplier = (float)_ailevel / 5f; });
            _eventsystem.Connect(ActionEvents.AILevelDown, (context) => { if (_aienabled && _ailevel > 0) _ailevel -= 1; _toppaddle.SpeedMultiplier = (float)_ailevel / 5f; });
        }
        public void Run()
        {
            _frameclock = new Clock();
            while (_window.IsOpen())
            {
                _map.Update(_window);
                _map.InvokeCallbacks(_eventsystem, _window);
                Update();
                _window.Clear(Color.White);
                Draw();
                _window.Display();
            }
        }
        private void Update()
        {
            _frametime += _frameclock.Restart();
            while (_frametime >= Consts.TimeStep)
            {
                _frametime -= Consts.TimeStep;
                if (!_aienabled) _toppaddle.Update(Consts.TimeStep);
                _bottompaddle.Update(Consts.TimeStep);
                _ball.Update(Consts.TimeStep, _toppaddle, _bottompaddle);
                if (_aienabled)
                {
                    CalculateAIDirection();
                    _toppaddle.Update(Consts.TimeStep);
                }
            }
            if (_aienabled)
            {
                _topplayertext.DisplayedString = "Computer [" + _ailevel + "] 2:  " + _toppaddle.Score + " PTS   |   Toggle: 'Asterisk' - Level: 'Num+' / 'Num-'";
            }
            else
            {
                _topplayertext.DisplayedString = "Player 2:  " + _toppaddle.Score + " PTS   |   Movement: 'W' / 'D'";
            }
            _bottomplayertext.DisplayedString = "Player 1:  " + _bottompaddle.Score + " PTS   |   Movement: 'Left Arrow' / 'Right Arrow'";
        }
        private void CalculateAIDirection()
        {
            // This could also be improved a bit
            // All we do at the moment is find the projected ball angle and if that is in range move to it else move to match the player's paddle
            float deviation = 5;
            float ballangle = _ball.ProjectedAngle;
            if (ballangle > _toppaddle.MaximumRotation + 45 || ballangle < _toppaddle.MinimumRotation - 45) { ballangle = _bottompaddle.Position - 180; deviation = 35; }
            if (ballangle + deviation >= _toppaddle.Position && ballangle - deviation <= _toppaddle.Position) _toppaddle.Direction = RotationDirection.None;
            else if (ballangle > _toppaddle.Position) _toppaddle.Direction = RotationDirection.Clockwise;
            else if (ballangle < _toppaddle.Position) _toppaddle.Direction = RotationDirection.CounterClockwise;
            else _toppaddle.Direction = RotationDirection.None;
        }
        private void Draw()
        {
            _window.Draw(_background);
            _window.Draw(_toppaddle);
            _window.Draw(_bottompaddle);
            _window.Draw(_ball);
            _window.Draw(_topplayertext);
            _window.Draw(_bottomplayertext);
        }
        #endregion
    }
}
