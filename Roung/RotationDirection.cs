﻿using System;

namespace Roung
{
    public enum RotationDirection
    {
        None,
        Clockwise,
        CounterClockwise
    }
}
