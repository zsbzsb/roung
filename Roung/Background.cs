﻿using System;
using SFML.Window;
using SFML.Graphics;

namespace Roung
{
    public class Background : Drawable
    {
        #region Variables
        private CircleShape _rotationcircle = null;
        private CircleShape _boundscircle = null;
        #endregion

        #region Constructors
        public Background()
        {
            _rotationcircle = new CircleShape(Consts.PlayingFieldSize / 2, 375);
            _rotationcircle.Origin = new Vector2f(_rotationcircle.Radius, _rotationcircle.Radius);
            _rotationcircle.Position = Consts.CenterField;
            _rotationcircle.FillColor = Color.Transparent;
            _rotationcircle.OutlineThickness = 3.5f;
            _rotationcircle.OutlineColor = new Color(125, 125, 255);
            _boundscircle = new CircleShape((Consts.PlayingFieldSize / 2) + Consts.BallSize + 20, 375);
            _boundscircle.Origin = new Vector2f(_boundscircle.Radius, _boundscircle.Radius);
            _boundscircle.Position = Consts.CenterField;
            _boundscircle.FillColor = Color.Transparent;
            _boundscircle.OutlineThickness = 3.5f;
            _boundscircle.OutlineColor = Color.Red;
        }
        #endregion

        #region Functions
        public void Draw(RenderTarget target, RenderStates states)
        {
            target.Draw(_rotationcircle, states);
            target.Draw(_boundscircle, states);
        }
        #endregion
    }
}
