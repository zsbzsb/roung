﻿using System;

namespace Roung
{
    public enum ActionEvents
    {
        WindowClosed,
        TopPaddleRight,
        TopPaddleLeft,
        TopPaddleStop,
        BottomPaddleRight,
        BottomPaddleLeft,
        BottomPaddleStop,
        AIToggle,
        AILevelUp,
        AILevelDown
    }
}
