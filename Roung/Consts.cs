﻿using System;
using SFML.Window;
using NetEXT.TimeFunctions;

namespace Roung
{
    public static class Consts
    {
        public static readonly Vector2u WindowSize = new Vector2u(800, 730);
        public static readonly float PlayingFieldSize = 500;
        public static readonly Vector2f CenterField = new Vector2f(400, 365);
        public static readonly float BallSize = 40;
        public static readonly Vector2f PaddleSize = new Vector2f(100, 5);
        public static readonly float BaseRotationSpeed = 50;
        public static Time TimeStep = Time.FromSeconds(1d / 60d);
    }
}
