﻿using System;
using SFML.Window;
using SFML.Graphics;
using NetEXT.Vectors;
using NetEXT.TimeFunctions;

namespace Roung
{
    public class Paddle : Drawable
    {
        #region Variables
        private float _position = 0;
        private float _defaultposition = 0;
        private float _minimumrotation = 0;
        private float _maximumrotation = 360;
        private RectangleShape _paddleshape = null;
        private PolarVector _positioncalculator = new PolarVector(Consts.PlayingFieldSize / 2, 0);
        private RotationDirection _direction = RotationDirection.None;
        private int _score = 0;
        private float _speedmultiplier = 1;
        private float _bouncemultiplier = 1;
        #endregion

        #region Properties
        public float Position
        {
            get
            {
                return _position;
            }
            set
            {
                _position = value;
            }
        }
        public float MinimumRotation
        {
            get
            {
                return _minimumrotation;
            }
            set
            {
                _minimumrotation = value;
            }
        }
        public float MaximumRotation
        {
            get
            {
                return _maximumrotation;
            }
            set
            {
                _maximumrotation = value;
            }
        }
        public RotationDirection Direction
        {
            get
            {
                return _direction;
            }
            set
            {
                _direction = value;
            }
        }
        public int Score
        {
            get
            {
                return _score;
            }
            set
            {
                _score = value;
            }
        }
        public float SpeedMultiplier
        {
            get
            {
                return _speedmultiplier;
            }
            set
            {
                _speedmultiplier = value;
            }
        }
        public float BounceMultiplier
        {
            get
            {
                return _bouncemultiplier;
            }
            set
            {
                _bouncemultiplier = value;
            }
        }
        #endregion

        #region Constructors
        public Paddle(float DefaultPosition)
        {
            _defaultposition = DefaultPosition;
            _position = DefaultPosition;
            _paddleshape = new RectangleShape(Consts.PaddleSize);
            _paddleshape.Origin = new Vector2f(Consts.PaddleSize.X / 2, Consts.PaddleSize.Y / 2);
            _paddleshape.FillColor = Color.Black;
        }
        #endregion

        #region Functions
        public void Update(Time DeltaTime)
        {
            if (_direction == RotationDirection.Clockwise)
            {
                _position = (float)Math.Min(_maximumrotation, _position + (DeltaTime.Seconds * Consts.BaseRotationSpeed * _speedmultiplier * _bouncemultiplier));
            }
            else if (_direction == RotationDirection.CounterClockwise)
            {
                _position = (float)Math.Max(_minimumrotation, _position - (DeltaTime.Seconds * Consts.BaseRotationSpeed * _speedmultiplier * _bouncemultiplier));
            }
        }
        public void Draw(RenderTarget target, RenderStates states)
        {
            UpdateShapePosition();
            target.Draw(_paddleshape, states);
        }
        private void UpdateShapePosition()
        {
            _positioncalculator.A = _position - 90;
            _paddleshape.Position = _positioncalculator + Consts.CenterField;
            _paddleshape.Rotation = _position;
        }
        public void Reset()
        {
            _direction = RotationDirection.None;
            _position = _defaultposition;
            _bouncemultiplier = 1;
        }
        #endregion
    }
}
