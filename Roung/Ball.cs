﻿using System;
using SFML.Window;
using SFML.Graphics;
using NetEXT.Vectors;
using NetEXT.TimeFunctions;
using NetEXT.MathFunctions;

namespace Roung
{
    public class Ball : Drawable
    {
        #region Variables
        private PolarVector _movement = new PolarVector(80, 0);
        private CircleShape _ballshape = null;
        private Paddle _lastbouncer = null;
        private float _projectedangle = 0;
        private bool _projectedneedupdate = true;
        #endregion

        #region Properties
        public Vector2f Position
        {
            get
            {
                return _ballshape.Position;
            }
            set
            {
                _ballshape.Position = value;
            }
        }
        public PolarVector Movement
        {
            get
            {
                return _movement;
            }
        }
        public float ProjectedAngle
        {
            get
            {
                if (_projectedneedupdate) CalculateProjection();
                return _projectedangle;
            }
        }
        #endregion

        #region Constructors
        public Ball()
        {
            _ballshape = new CircleShape(Consts.BallSize / 2, 60);
            _ballshape.Origin = new Vector2f(Consts.BallSize / 2, Consts.BallSize / 2);
            _ballshape.FillColor = Color.Green;
            _ballshape.OutlineThickness = 1;
            _ballshape.OutlineColor = Color.Black;
            Reset();
        }
        #endregion

        #region Functions
        public void Update(Time DeltaTime, Paddle PaddleA, Paddle PaddleB) // This is where our collision happens with our paddles - Note the following function is pure magic known only by the greatest nerds of the entire world! [Do not try to understand this, seriously, not.]
        {
            Vector2f newpos = _ballshape.Position + ((Vector2f)_movement * (float)DeltaTime.Seconds); // Get our new postion
            float newang = _movement.A; // Create a variable for a possibly new angle
            float dist = Distance(Consts.CenterField, newpos); // Calculate the distance from the center of the field to the ball
            bool didreset = false;
            if (dist > Consts.PlayingFieldSize / 2 - Consts.BallSize / 2 && dist < Consts.PlayingFieldSize / 2 - Consts.BallSize / 4) // Determine if the ball falls within distance range of the paddle's rotation
            {
                float ang = AngleFromCenter(newpos); // Calculate the angle from the center of the field to the ball
                if (PaddleA != _lastbouncer) // If we didn't bounce off this paddle the last time check for collision
                {
                    if (ang > PaddleA.Position - 16f && ang < PaddleA.Position + 16f) // Todo figure out real angle deviations - Anyways we are determining if the angle of the ball lies within the angle of the paddle
                    {
                        float off = Math.Abs(PaddleA.Position - ang) / 5; // Todo figure out a more precise calculation - what we are doing is getting a number signifying how far off the center of the paddle
                        if (dist - off > 230) // Determine if our distance minus the center of the paddle is within collision range - this helps with the flat paddle on a curved collision
                        {
                            newang += 180 + ((PaddleA.Position - ang) * Math.Min(off, 1.5f)); // Bounce our ball off the paddle - taking into consideration the deviation from the center line of the paddle to create more bounce from the edges of the paddle
                            _lastbouncer = PaddleA;
                            _movement.R = Math.Min(_movement.R + 5.5f, 350); // Increase ball speed slightly on every bounce
                            PaddleA.BounceMultiplier = Math.Min(PaddleA.BounceMultiplier + .1f, 2.75f); // Increate paddle speed slightly
                            PaddleB.BounceMultiplier = Math.Min(PaddleB.BounceMultiplier + .1f, 2.75f);
                        }
                    }
                }
                if (PaddleB != _lastbouncer)
                {
                    if (ang > PaddleB.Position - 16.5f && ang < PaddleB.Position + 16.5f)
                    {
                        float off = Math.Abs(PaddleB.Position - ang) / 5;
                        if (dist - off > 230)
                        {
                            newang += 180 + ((PaddleB.Position - ang) * Math.Min(off, 1.5f));
                            _lastbouncer = PaddleB;
                            _movement.R = Math.Min(_movement.R + 5.5f, 350);
                            PaddleA.BounceMultiplier = Math.Min(PaddleA.BounceMultiplier + .1f, 2.75f);
                            PaddleB.BounceMultiplier = Math.Min(PaddleB.BounceMultiplier + .1f, 2.75f);
                        }
                    }
                }
            }
            else if (dist > (Consts.PlayingFieldSize / 2) + Consts.BallSize + 20)
            {
                // Score
                if (_lastbouncer != null)
                {
                    _lastbouncer.Score += 1;
                    _lastbouncer = null;
                }
                Reset();
                PaddleA.Reset();
                PaddleB.Reset();
                didreset = true;
            }
            if (!didreset)
            {
                // No reset? Move our ball
                if (_movement.A != newang)
                {
                    _movement.A = newang;
                    _projectedneedupdate = true;
                }
                _ballshape.Position = newpos;
            }
        }
        public void Reset()
        {
            _movement.R = 80f;
            _ballshape.Position = Consts.CenterField;
            _projectedneedupdate = true;
            if (RandomGenerator.Random(1, 100) <= 50)
            {
                _movement.A = RandomGenerator.Random(-40, 40) - 90;
            }
            else
            {
                _movement.A = RandomGenerator.Random(140, 220) - 90;
            }
        }
        public void Draw(RenderTarget target, RenderStates states)
        {
            target.Draw(_ballshape, states);
        }
        private float AngleFromCenter(Vector2f Position)
        {
            return Angle(Consts.CenterField, Position) + 90;
        }
        private float Distance(Vector2f A, Vector2f B)
        {
            return (float)Trigonometry.SqrRoot((B.X - A.X) * (B.X - A.X) + (B.Y - A.Y) * (B.Y - A.Y));
        }
        private float Angle(Vector2f A, Vector2f B)
        {
            return (float)Trigonometry.ArcTan2(B.Y - A.Y, B.X - A.X);
        }
        private void CalculateProjection()
        {
            // We will simply brute force this projection to keep this simple
            // This means we will be simulating the movement until we reach a point on the outside playing field
            // However this "could" be updated so that we don't need the brute force method
            Vector2f position = _ballshape.Position;
            while (Distance(Consts.CenterField, position) < Consts.PlayingFieldSize / 2)
            {
                position += ((Vector2f)_movement * (float)Consts.TimeStep.Seconds);
            }
            _projectedangle = AngleFromCenter(position);
            _projectedneedupdate = false;
        }
        #endregion
    }
}
