Roung -- This software is provided 'as-is' without any express or
implied warranty. Read the full license agreement in "license.txt".

Roung was written by Zachariah Brown (zsbzsb). It is a basic 'Pong' style game with paddles that rotate. It was designed to show a way to make circular 'Pong' with collision detection.